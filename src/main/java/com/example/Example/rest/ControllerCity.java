package com.example.Example.rest;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Value;
import com.example.Example.rest.bean.City;
import com.example.Example.rest.service.CityService;
import org.springframework.context.annotation.PropertySource;
@RestController
@RequestMapping("/controller")
@PropertySource("classpath:/constraints.properties")
public class ControllerCity 
{
	@Value("${message.default.welcome}")
    private String welcomeMessage;

    @Value("${message.default.goodbye}")
    private String goodBye;

	@Value("#{'${server.id}'.split(',')}")
	private List<Integer> serverId;

	@Value("${minVal}")
    private long minValue=0l;
    
	@Value("${maxVal}")
    private long maxValue=0l;
	
	@Autowired
	CityService cityService; 
	
	@RequestMapping(value="/welcome", method = RequestMethod.GET)
    public String welcome() {
        return welcomeMessage;
    }
	
	@RequestMapping(value="/minMax", method = RequestMethod.GET)
    public String minMax() {
        return "{\"minimo\":"+minValue+",\"maximo\":"+maxValue+"}";
    }
	
	@RequestMapping(value="/bye", method = RequestMethod.GET)
    public String bye() {
        return goodBye;
    }
	@RequestMapping(value="/serverId", method = RequestMethod.GET)
    public String serverId() 
	{
		String cadena="Example ";
		System.out.println(serverId.size());
		for(Integer temp : serverId)
		{
			System.out.println(temp);
			cadena = cadena + temp +" , ";
		}
        return cadena;
    }
	
	@RequestMapping(value = "/obtieneCiudades", method = RequestMethod.GET)
    public ResponseEntity<List<City>> obtieneCiudades() 
    {
        List<City> ciudades = cityService.findAll();
        return new ResponseEntity<List<City>>(ciudades, HttpStatus.OK);
    }
	@RequestMapping(value = "/borrarCiudad/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteCity(@PathVariable("id") long id) 
    { 
    	City delete1=new City();
    	delete1.setId(id); 
        return new ResponseEntity<String>((new Boolean(true)).toString(), HttpStatus.OK);
    }
}