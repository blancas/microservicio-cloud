package com.example.Example.rest.service;
import java.util.List;
import com.example.Example.rest.bean.City;
public interface ICityService 
{	
    public List<City> findAll();
}
